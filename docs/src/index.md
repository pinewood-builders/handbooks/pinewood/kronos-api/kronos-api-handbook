---
home: true
heroImage: /logo.png
tagline: The documentation about the KronosAPI for the Kronos discord bot and Pinewood
actionText: Quick Start →
actionLink: /api/
features:
- title: Request a divisons schedule
  details: See the schedule of a divison directly from the source that it comes from!
- title: See who's blacklisted
  details: Get information about blacklisted users in seperate divisions.
- title: Make smartlog requests?
  details: Not sure about this one yet.
footer: Made by Stefano Haagmans with ❤️
---

:::danger Almost all endpoints need a API Key
This API has been built, to be mostly for private use within Pinewood, or connections with Pinewood that would be allowed by Coaster.
Coaster has full say over who has access to any of these endpoints, and who doesn't.

If you'd like to request an API key, please contact Coasterteam on the Development Den.
:::